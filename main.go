package main

import (
	"main/handler"
	"net/http"

	_ "github.com/go-sql-driver/mysql"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
	}))
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// Admin Function
	// Admin customer
	e.GET("/getcustomer", handler.Getcustomer)
	e.POST("/getcustomerbyid", handler.Getcustomerbyid)
	e.POST("/addcustomer", handler.Addcustomer)
	e.POST("/updatecustomer", handler.Updatecustomer)
	e.POST("/deletecustomer", handler.Deletecustomerbyid)
	// Admin employee
	e.GET("/getemployee", handler.Getemployee)
	e.POST("/getemployeebyid", handler.Getemployeebyid)
	e.POST("/addemployee", handler.Addemployee)
	e.POST("/updateemployee", handler.Updateemployee)
	e.POST("/deleteemployee", handler.Deleteemployeebyid)
	e.POST("/getemployeebyuserpass", handler.Getemployeebyuserpass)
	// Port run
	e.Logger.Fatal(e.Start(":7001"))
}
