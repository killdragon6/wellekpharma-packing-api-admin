package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/labstack/echo"
)

var DB *sql.DB

func Initialize() {
	db, err := sql.Open("mysql", "root:@/wellekpharma_packing_system")
	if err != nil {
		log.Fatal(err)
	} else {
		fmt.Printf("connectDB")
	}
	DB = db
}

func Getcustomer(c echo.Context) interface{} {
	Initialize()
	var array []interface{}
	query := "select * from admin_customer"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var customer_id string
		var customer_name string
		var customer_address string
		var inline_sequence string
		var line string
		var phone_number_1 string
		var phone_number_2 string
		var phone_number_3 string
		var latitude string
		var longitude string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&customer_id,
			&customer_name,
			&customer_address,
			&inline_sequence,
			&line,
			&phone_number_1,
			&phone_number_2,
			&phone_number_3,
			&latitude,
			&longitude,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["customer_address"] = customer_address
		elements["inline_sequence"] = inline_sequence
		elements["line"] = line
		elements["phone_number_1"] = phone_number_1
		elements["phone_number_2"] = phone_number_2
		elements["phone_number_3"] = phone_number_3
		elements["latitude"] = latitude
		elements["longitude"] = longitude
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Getcustomerbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from admin_customer where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var customer_id string
		var customer_name string
		var customer_address string
		var inline_sequence string
		var line string
		var phone_number_1 string
		var phone_number_2 string
		var phone_number_3 string
		var latitude string
		var longitude string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&customer_id,
			&customer_name,
			&customer_address,
			&inline_sequence,
			&line,
			&phone_number_1,
			&phone_number_2,
			&phone_number_3,
			&latitude,
			&longitude,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["customer_address"] = customer_address
		elements["inline_sequence"] = inline_sequence
		elements["line"] = line
		elements["phone_number_1"] = phone_number_1
		elements["phone_number_2"] = phone_number_2
		elements["phone_number_3"] = phone_number_3
		elements["latitude"] = latitude
		elements["longitude"] = longitude
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Addcustomer(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()
	// json_map := make(map[string]interface{})

	var array []interface{}
	// query :=
	rows, err := DB.Query("INSERT INTO admin_customer(customer_id, customer_name, customer_address, inline_sequence, line, phone_number_1, phone_number_2,phone_number_3,latitude,longitude,create_date,create_at,modify_date,modify_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["customer_id"], json_map["customer_name"], json_map["customer_address"], json_map["inline_sequence"], json_map["line"], json_map["phone_number_1"], json_map["phone_number_2"], json_map["phone_number_3"], json_map["latitude"], json_map["longitude"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var customer_id string
		var customer_name string
		var customer_address string
		var inline_sequence string
		var line string
		var phone_number_1 string
		var phone_number_2 string
		var phone_number_3 string
		var latitude string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&customer_id,
			&customer_name,
			&customer_address,
			&inline_sequence,
			&line,
			&phone_number_1,
			&phone_number_2,
			&phone_number_3,
			&latitude,
			&latitude,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["customer_address"] = customer_address
		elements["inline_sequence"] = inline_sequence
		elements["line"] = line
		elements["phone_number_1"] = phone_number_1
		elements["phone_number_2"] = phone_number_2
		elements["phone_number_3"] = phone_number_3
		elements["latitude"] = latitude
		elements["latitude"] = latitude
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res
}

func Updatecustomer(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	// json_map := make(map[string]interface{})

	var array []interface{}
	var stringupdate string
	if json_map["customer_id"] != nil {
		stringupdate += "customer_id = '" + json_map["customer_id"].(string) + "',"
	}
	if json_map["customer_name"] != nil {
		stringupdate += "customer_name = '" + json_map["customer_name"].(string) + "',"
	}
	if json_map["customer_address"] != nil {
		stringupdate += "customer_address = '" + json_map["customer_address"].(string) + "',"
	}
	if json_map["inline_sequence"] != nil {
		stringupdate += "inline_sequence = " + json_map["inline_sequence"].(string) + ","
	}
	if json_map["line"] != nil {
		stringupdate += "line = " + json_map["line"].(string) + ","
	}
	if json_map["phone_number_1"] != nil {
		stringupdate += "phone_number_1 = '" + json_map["phone_number_1"].(string) + "',"
	}
	if json_map["phone_number_2"] != nil {
		stringupdate += "phone_number_2 = '" + json_map["phone_number_2"].(string) + "',"
	}
	if json_map["phone_number_3"] != nil {
		stringupdate += "phone_number_3 = '" + json_map["phone_number_3"].(string) + "',"
	}
	if json_map["latitude"] != nil {
		stringupdate += "latitude = '" + json_map["latitude"].(string) + "',"
	}
	if json_map["longitude"] != nil {
		stringupdate += "longitude = '" + json_map["longitude"].(string) + "',"
	}
	if json_map["create_date"] != nil {
		stringupdate += "create_date = '" + json_map["create_date"].(string) + "',"
	}
	if json_map["create_at"] != nil {
		stringupdate += "create_at = '" + json_map["create_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}

	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE admin_customer
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	// INSERT INTO admin_customer(customer_id, customer_name, customer_address, inline_sequence, line, phone_number_1, phone_number_2,phone_number_3,latitude,longitude,create_date,create_at,modify_date,modify_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["customer_id"], json_map["customer_name"], json_map["customer_address"], json_map["inline_sequence"], json_map["line"], json_map["phone_number_1"], json_map["phone_number_2"], json_map["phone_number_3"], json_map["latitude"], json_map["longitude"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"]
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var customer_id string
		var customer_name string
		var customer_address string
		var inline_sequence string
		var line string
		var phone_number_1 string
		var phone_number_2 string
		var phone_number_3 string
		var latitude string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&customer_id,
			&customer_name,
			&customer_address,
			&inline_sequence,
			&line,
			&phone_number_1,
			&phone_number_2,
			&phone_number_3,
			&latitude,
			&latitude,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["customer_id"] = customer_id
		elements["customer_name"] = customer_name
		elements["customer_address"] = customer_address
		elements["inline_sequence"] = inline_sequence
		elements["line"] = line
		elements["phone_number_1"] = phone_number_1
		elements["phone_number_2"] = phone_number_2
		elements["phone_number_3"] = phone_number_3
		elements["latitude"] = latitude
		elements["latitude"] = latitude
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res
}

func Deletecustomerbyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	sqlStatement := `
	DELETE FROM admin_customer
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	res := make(map[string]interface{})
	res["status"] = true

	return res
}

// Admin employee
func Getemployee(c echo.Context) interface{} {
	Initialize()

	var array []interface{}
	query := "select * from admin_employee"
	rows, err := DB.Query(query)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var department string
		var merchandising_department string
		var employee_id string
		var f_name string
		var s_name string
		var phone_number string
		var password string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var position string

		rows.Scan(&id,
			&department,
			&merchandising_department,
			&employee_id,
			&f_name,
			&s_name,
			&phone_number,
			&password,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&position,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["department"] = department
		elements["merchandising_department"] = merchandising_department
		elements["employee_id"] = employee_id
		elements["f_name"] = f_name
		elements["s_name"] = s_name
		elements["phone_number"] = phone_number
		elements["password"] = password
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["position"] = position
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Getemployeebyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from admin_employee where id = ?", json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var department string
		var merchandising_department string
		var employee_id string
		var f_name string
		var s_name string
		var phone_number string
		var password string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var position string

		rows.Scan(&id,
			&department,
			&merchandising_department,
			&employee_id,
			&f_name,
			&s_name,
			&phone_number,
			&password,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&position,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["department"] = department
		elements["merchandising_department"] = merchandising_department
		elements["employee_id"] = employee_id
		elements["f_name"] = f_name
		elements["s_name"] = s_name
		elements["phone_number"] = phone_number
		elements["password"] = password
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["position"] = position
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func Addemployee(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var array []interface{}
	// query :=
	rows, err := DB.Query("INSERT INTO admin_employee(`department`, `merchandising_department`, `position`, `employee_id`, `f_name`, `s_name`, `phone_number`, `password`, `create_date`, `create_at`, `modify_date`, `modify_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)", json_map["department"], json_map["merchandising_department"], json_map["position"], json_map["employee_id"], json_map["f_name"], json_map["s_name"], json_map["phone_number"], json_map["password"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var department string
		var position string
		var merchandising_department string
		var employee_id string
		var f_name string
		var s_name string
		var phone_number string
		var password string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&department,
			&merchandising_department,
			&employee_id,
			&f_name,
			&s_name,
			&phone_number,
			&password,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["department"] = department
		elements["position"] = position
		elements["merchandising_department"] = merchandising_department
		elements["employee_id"] = employee_id
		elements["f_name"] = f_name
		elements["s_name"] = s_name
		elements["phone_number"] = phone_number
		elements["password"] = password
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res
}

func Updateemployee(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var array []interface{}
	var stringupdate string
	if json_map["department"] != nil {
		stringupdate += "department = '" + json_map["department"].(string) + "',"
	}
	if json_map["merchandising_department"] != nil {
		stringupdate += "merchandising_department = '" + json_map["merchandising_department"].(string) + "',"
	}
	if json_map["employee_id"] != nil {
		stringupdate += "employee_id = '" + json_map["employee_id"].(string) + "',"
	}
	if json_map["position"] != nil {
		stringupdate += "position = '" + json_map["position"].(string) + "',"
	}
	if json_map["f_name"] != nil {
		stringupdate += "f_name = '" + json_map["f_name"].(string) + "',"
	}
	if json_map["s_name"] != nil {
		stringupdate += "s_name = '" + json_map["s_name"].(string) + "',"
	}
	if json_map["phone_number"] != nil {
		stringupdate += "phone_number = '" + json_map["phone_number"].(string) + "',"
	}
	if json_map["password"] != nil {
		stringupdate += "password = '" + json_map["password"].(string) + "',"
	}
	if json_map["create_date"] != nil {
		stringupdate += "create_date = '" + json_map["create_date"].(string) + "',"
	}
	if json_map["create_at"] != nil {
		stringupdate += "create_at = '" + json_map["create_at"].(string) + "',"
	}
	if json_map["modify_date"] != nil {
		stringupdate += "modify_date = '" + json_map["modify_date"].(string) + "',"
	}
	if json_map["modify_at"] != nil {
		stringupdate += "modify_at = '" + json_map["modify_at"].(string) + "',"
	}

	stringupdate = trimLastChar(stringupdate)
	sqlStatement := `
	UPDATE admin_employee
	SET ` + stringupdate + `
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	// INSERT INTO admin_customer(customer_id, customer_name, customer_address, inline_sequence, line, phone_number_1, phone_number_2,phone_number_3,latitude,longitude,create_date,create_at,modify_date,modify_at) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)", json_map["customer_id"], json_map["customer_name"], json_map["customer_address"], json_map["inline_sequence"], json_map["line"], json_map["phone_number_1"], json_map["phone_number_2"], json_map["phone_number_3"], json_map["latitude"], json_map["longitude"], json_map["create_date"], json_map["create_at"], json_map["modify_date"], json_map["modify_at"]
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var department string
		var position string
		var merchandising_department string
		var employee_id string
		var f_name string
		var s_name string
		var phone_number string
		var password string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string

		rows.Scan(&id,
			&department,
			&position,
			&merchandising_department,
			&employee_id,
			&f_name,
			&s_name,
			&phone_number,
			&password,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["department"] = department
		elements["position"] = position
		elements["merchandising_department"] = merchandising_department
		elements["employee_id"] = employee_id
		elements["f_name"] = f_name
		elements["s_name"] = s_name
		elements["phone_number"] = phone_number
		elements["password"] = password
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		array = append(array, elements)
	}

	res := make(map[string]interface{})
	res["data"] = array
	return res
}

func Deleteemployeebyid(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	sqlStatement := `
	DELETE FROM admin_employee
	WHERE id = ?`
	rows, err := DB.Query(sqlStatement, json_map["id"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	res := make(map[string]interface{})
	res["status"] = true

	return res
}

func Getemployeebyuserpass(c echo.Context) interface{} {
	json_map := make(map[string]interface{})
	err := json.NewDecoder(c.Request().Body).Decode(&json_map)
	Initialize()

	var array []interface{}
	// query :=
	rows, err := DB.Query("select * from admin_employee where employee_id = ? and password = ?", json_map["employee_id"], json_map["password"])
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var id int
		var department string
		var merchandising_department string
		var employee_id string
		var f_name string
		var s_name string
		var phone_number string
		var password string
		var create_date string
		var create_at string
		var modify_date string
		var modify_at string
		var position string

		rows.Scan(&id,
			&department,
			&merchandising_department,
			&employee_id,
			&f_name,
			&s_name,
			&phone_number,
			&password,
			&create_date,
			&create_at,
			&modify_date,
			&modify_at,
			&position,
		)
		elements := make(map[string]interface{})
		elements["id"] = id
		elements["department"] = department
		elements["merchandising_department"] = merchandising_department
		elements["employee_id"] = employee_id
		elements["f_name"] = f_name
		elements["s_name"] = s_name
		elements["phone_number"] = phone_number
		elements["password"] = password
		elements["create_date"] = create_date
		elements["create_at"] = create_at
		elements["modify_date"] = modify_date
		elements["modify_at"] = modify_at
		elements["position"] = position
		array = append(array, elements)
	}
	res := make(map[string]interface{})
	res["data"] = array
	return res["data"]
}

func trimLastChar(s string) string {
	r, size := utf8.DecodeLastRuneInString(s)
	if r == utf8.RuneError && (size == 0 || size == 1) {
		size = 0
	}
	return s[:len(s)-size]
}
