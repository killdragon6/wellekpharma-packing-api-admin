package handler

import (
	"main/model"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

// Admin customer
func Getcustomer(c echo.Context) error {
	res := model.Getcustomer(c)
	return c.JSON(http.StatusOK, res)
}

func Getcustomerbyid(c echo.Context) error {
	res := model.Getcustomerbyid(c)
	return c.JSON(http.StatusOK, res)
}

func Addcustomer(c echo.Context) error {
	res := model.Addcustomer(c)
	return c.JSON(http.StatusOK, res)
}

func Updatecustomer(c echo.Context) error {
	res := model.Updatecustomer(c)
	return c.JSON(http.StatusOK, res)
}

func Deletecustomerbyid(c echo.Context) error {
	res := model.Deletecustomerbyid(c)
	return c.JSON(http.StatusOK, res)
}

// Admin employee
func Getemployee(c echo.Context) error {
	res := model.Getemployee(c)
	return c.JSON(http.StatusOK, res)
}

func Getemployeebyid(c echo.Context) error {
	res := model.Getemployeebyid(c)
	return c.JSON(http.StatusOK, res)
}

func Addemployee(c echo.Context) error {
	res := model.Addemployee(c)
	return c.JSON(http.StatusOK, res)
}

func Updateemployee(c echo.Context) error {
	res := model.Updateemployee(c)
	return c.JSON(http.StatusOK, res)
}

func Deleteemployeebyid(c echo.Context) error {
	res := model.Deleteemployeebyid(c)
	return c.JSON(http.StatusOK, res)
}

func Getemployeebyuserpass(c echo.Context) error {
	res := model.Getemployeebyuserpass(c)
	return c.JSON(http.StatusOK, res)
}
